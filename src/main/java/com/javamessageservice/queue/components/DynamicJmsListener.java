package com.javamessageservice.queue.components;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import org.springframework.stereotype.Component;

import com.javamessageservice.queue.entity.JmsQueue;
import com.javamessageservice.queue.services.JmsQueueService;

import jakarta.jms.JMSException;

import java.util.List;

@Component
public class DynamicJmsListener implements JmsListenerConfigurer {

    @Autowired
    private JmsQueueService jmsQueueService;

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
        List<JmsQueue> queues = jmsQueueService.getAllQueues();

        for (JmsQueue queue : queues) {
            SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
            endpoint.setId(queue.getQueueName() + "-listener");
            endpoint.setDestination(queue.getQueueName());
            endpoint.setMessageListener(message -> {
                try {
                    System.out.println("Received message from " + queue.getQueueName() + ": " + message);
                    System.out.println("Received message from " + queue.getQueueName() + ": " + message.getStringProperty("text"));
                } catch (JMSException e) {
                    System.out.println(e);
                    System.out.println("Received message from " + queue.getQueueName() + ": " + message);
                }
            });

            registrar.registerEndpoint(endpoint);
        }
    }
}
