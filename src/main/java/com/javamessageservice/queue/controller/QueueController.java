package com.javamessageservice.queue.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.javamessageservice.queue.services.JmsProducer;

@RestController
public class QueueController {
    @Autowired
    private JmsProducer jmsProducer;

    @GetMapping("/send-message/{message}/{queue}")
    public void sendMessage(@PathVariable("message") String message, @PathVariable("queue") String queue) {
      jmsProducer.sendMessage(message, queue);
    }

}
