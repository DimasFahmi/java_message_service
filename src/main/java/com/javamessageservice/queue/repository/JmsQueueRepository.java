package com.javamessageservice.queue.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.javamessageservice.queue.entity.JmsQueue;

@Repository
public interface JmsQueueRepository extends JpaRepository<JmsQueue, Long> {
}
