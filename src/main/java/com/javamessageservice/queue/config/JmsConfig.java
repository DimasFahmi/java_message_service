package com.javamessageservice.queue.config;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.Queue;
import jakarta.jms.Session;

@Configuration
public class JmsConfig {

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Value("${spring.activemq.user}")
    private String brokerUser;

    @Value("${spring.activemq.password}")
    private String brokerPassword;

    @Value("${custom.jms.queues}")
    private String[] queueNames;

    @Bean
    public ConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory(brokerUser, brokerPassword, brokerUrl);
    }

    @Bean
    public Queue[] queues(ConnectionFactory connectionFactory) throws JMSException {
        Session session = connectionFactory.createConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue[] queues = new Queue[queueNames.length];
        for (int i = 0; i < queueNames.length; i++) {
            queues[i] = session.createQueue(queueNames[i]);
        }
        return queues;
    }
}
