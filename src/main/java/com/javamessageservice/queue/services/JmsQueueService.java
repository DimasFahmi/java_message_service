package com.javamessageservice.queue.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.javamessageservice.queue.entity.JmsQueue;
import com.javamessageservice.queue.repository.JmsQueueRepository;

import java.util.List;

@Service
public class JmsQueueService {

    @Autowired
    private JmsQueueRepository jmsQueueRepository;

    public List<JmsQueue> getAllQueues() {
        return jmsQueueRepository.findAll();
    }
}
