# First stage: Build the application
FROM jelastic/maven:3.9.6-openjdk-23.ea-b18-almalinux-9 AS build

# Set the working directory for the build
WORKDIR /app

# Copy the pom.xml and dependencies files first to leverage Docker cache
COPY pom.xml .
COPY src ./src

# Package the application
RUN mvn clean package

# Second stage: Create the runtime image
# Use the official OpenJDK base image for Java 17
FROM alpine/java:22-jdk

# Set the working directory for the application
WORKDIR /app

# Copy the JAR file from the build stage
COPY --from=build /app/target/queue-0.0.1-SNAPSHOT.jar /app/queue-0.0.1-SNAPSHOT.jar

# Expose the port your application runs on
EXPOSE 2000

# Command to run the application
CMD ["java", "-jar", "/app/queue-0.0.1-SNAPSHOT.jar"]
